#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>
#include <cstdio>
#include<cstdlib>

void display();
void reshape(int,int);
void timer(int);

//for user camera

bool finish=false;bool balik=false;bool pressed=false;bool pressed_again=false;
bool idle=false;bool turn_again=false;bool animation_player=false;bool ready_go_back=false; // untuk giliran player

void glutBitmapString(const char *str,void *font){
	int i=0;
	while(str[i]!='\0')
		glutBitmapCharacter(font,str[i++]); // prints the string in this format with times font
}

char *to_string(int num){
    char string[20];
	itoa(num,string,10);
	return string;
}

float Anchor_board[3]={-10,-9.3,-20};
float titik_tengah[100][5];// center of triangle XP YP ZP Baris_ke Up

int teleport[100];
int teleport_point[]={3,18,77,51,39,83,28,68,97,91}; // add 1 in board
int arraySize = sizeof(teleport_point)/sizeof(teleport_point[0]);
void teleport_settings(){
        for(int i = 0; i< 100; i++){
            teleport[i]=0;
        }
        for(int i = 0; i<arraySize; i++)
            {
                teleport[teleport_point[i]]=1;
            }
}

int time_idle_animate;
float user_x_angle_rotate=0.0; //negatif benda bergerak ke bawah positif bergerak ke atas
float user_y_angle_rotate=0.0; // positif bergerak ke kiri negatif bererak ke kanan
float user_z_angle_rotate=0.0; // positif putar ke kanan negatif putar ke kiri
void Putar_Arena(){ //for idle animation
    glTranslatef(10,10,-5); //make center of gravity for rotate
    glRotatef(user_x_angle_rotate,1.0,0.0,0.0);
    glRotatef(user_y_angle_rotate,0.0,1.0,0.0);
    glRotatef(user_z_angle_rotate,0.0,0.0,1.0);
    glTranslatef(-10,-10,5);
}

void board(){
    glTranslatef(Anchor_board[0],Anchor_board[1],Anchor_board[2]); //start coordinate for triangle
     //back with start of triangle
    if(idle==true)
        Putar_Arena();

    float color[6][3]={{0.5,0.0,0.0},{0.0,0.5,0.0},{0.0,0.0,0.5},{0.5,0.5,0.0},{0.5,0.0,0.5},{0.0,0.5,0.5}};
    int counter_color=0;
    int counter_board = 0;
    int batas = 18; //0-19
    int tinggi = 9; //0-9
    int kanan_kiri=0;
    float x_start=-1.0,y_start=0.0; // 0.0 2.0 first position x y
    for(int i = 0; i<= tinggi; i++){ //loop sampai tinggi
        kanan_kiri=kanan_kiri%2;
        if(kanan_kiri==0)
        {
            x_start=x_start+1; //titik tengah acuan
            y_start=y_start+2; //titik tengah acuan

            //1 adalah 1/2 alas segitiga dan 2 adalah tinggi segitiga
            for(int j = 0; j<= batas; j++) //looping samapi batas
            {
                titik_tengah[counter_board][3]=i;
                if (j==batas)
                    titik_tengah[counter_board][4]=1;
                else
                    titik_tengah[counter_board][4]=0;

                int s=counter_color%6;
                if(j%2==1)
                    {
                    titik_tengah[counter_board][0]=x_start+1;
                    titik_tengah[counter_board][1]=y_start;
                    titik_tengah[counter_board][2]=-i;

                    if(teleport[counter_board]==1)
                    {
                    glColor3f(0.0f, 0.0f, 0.0f);//Lilac
                    glBegin(GL_QUADS);
                    //SERONG KANAN x
                    glVertex3f(x_start+0.7,y_start-0.8,-i);
                    glVertex3f(x_start+0.9,y_start-0.8,-i);
                    glVertex3f(x_start+1.3,y_start-1.3,-i);
                    glVertex3f(x_start+1.1,y_start-1.3,-i);
                     //SERONG KIRI x
                    glVertex3f(x_start+1.1,y_start-0.8,-i);
                    glVertex3f(x_start+1.3,y_start-0.8,-i);
                    glVertex3f(x_start+0.9,y_start-1.3,-i);
                    glVertex3f(x_start+0.7,y_start-1.3,-i);
                    glEnd();
                    }

                    //segitiga terbalik
                    glColor3f(0.0,0.5,0.0);
                    glColor3f(color[s][0],color[s][1],color[s][2]);
                    glBegin(GL_TRIANGLES); //tutup
                    glVertex3f(x_start,y_start,-i);
                    glVertex3f(x_start+1,y_start-2,-i);
                    glVertex3f(x_start+2,y_start,-i);
                    glEnd();
                    glBegin(GL_TRIANGLES); //alas
                    glVertex3f(x_start,y_start,-i-1);
                    glVertex3f(x_start+1,y_start-2,-i-1);
                    glVertex3f(x_start+2,y_start,-i-1);
                    glEnd();
                    glBegin(GL_QUADS);
                    glVertex3f(x_start,y_start,-i);
                    glVertex3f(x_start,y_start,-i-1);
                    glVertex3f(x_start+1,y_start-2,-i-1);
                    glVertex3f(x_start+1,y_start-2,-i);
                    glEnd();
                    glBegin(GL_QUADS);
                    glVertex3f(x_start+1,y_start-2,-i);
                    glVertex3f(x_start+1,y_start-2,-i-1);
                    glVertex3f(x_start+2,y_start,-i-1);
                    glVertex3f(x_start+2,y_start,-i);
                    glEnd();
                    glBegin(GL_QUADS);
                    glVertex3f(x_start+2,y_start,-i);
                    glVertex3f(x_start+2,y_start,-i-1);
                    glVertex3f(x_start,y_start,-i-1);
                    glVertex3f(x_start,y_start,-i);
                    glEnd();
                    if(idle==false)
                        {//number board
                            glColor3f(1.0f, 1.0f, 1.0f);//Lilac
                            glRasterPos3f(0,0,0);
                            glRasterPos3f(x_start-0.27+1,y_start-1.30,-i+0.0001);
                            void* font = GLUT_BITMAP_HELVETICA_10;
                            glutBitmapString(to_string(counter_board+1),font);
                        }
                    x_start=x_start+2;
                    }
                else
                {
                titik_tengah[counter_board][0]=x_start;
                titik_tengah[counter_board][1]=y_start;
                titik_tengah[counter_board][2]=-i;

                if(teleport[counter_board]==1)
                {
                glColor3f(0.0f, 0.0f, 0.0f);//Lilac
                glBegin(GL_QUADS);
                //SERONG KANAN x
                glVertex3f(x_start+0.3,y_start-0.8,-i);
                glVertex3f(x_start+0.1,y_start-0.8,-i);
                glVertex3f(x_start-0.1,y_start-1.3,-i);
                glVertex3f(x_start-0.3,y_start-1.3,-i);
                 //SERONG KIRI x
                glVertex3f(x_start-0.3,y_start-0.8,-i);
                glVertex3f(x_start-0.1,y_start-0.8,-i);
                glVertex3f(x_start+0.1,y_start-1.3,-i);
                glVertex3f(x_start+0.3,y_start-1.3,-i);
                glEnd();
                }

                //segitiga biasa
                glColor3f(0.5,0.0,0.0);
                glColor3f(color[s][0],color[s][1],color[s][2]);
                glBegin(GL_TRIANGLES); //tutup
                glVertex3f(x_start,y_start,-i);
                glVertex3f(x_start-1,y_start-2,-i);
                glVertex3f(x_start+1,y_start-2,-i);
                glEnd();
                glBegin(GL_TRIANGLES); //alas
                glVertex3f(x_start,y_start,-i-1);
                glVertex3f(x_start-1,y_start-2,-i-1);
                glVertex3f(x_start+1,y_start-2,-i-1);
                glEnd();
                glBegin(GL_QUADS);
                glVertex3f(x_start,y_start,-i);
                glVertex3f(x_start,y_start,-i-1);
                glVertex3f(x_start-1,y_start-2,-i-1);
                glVertex3f(x_start-1,y_start-2,-i);
                glEnd();
                glBegin(GL_QUADS);
                glVertex3f(x_start-1,y_start-2,-i);
                glVertex3f(x_start-1,y_start-2,-i-1);
                glVertex3f(x_start+1,y_start-2,-i-1);
                glVertex3f(x_start+1,y_start-2,-i);
                glEnd();
                glBegin(GL_QUADS);
                glVertex3f(x_start+1,y_start-2,-i);
                glVertex3f(x_start+1,y_start-2,-i-1);
                glVertex3f(x_start,y_start,-i-1);
                glVertex3f(x_start,y_start,-i);
                glEnd();
                if(idle==false)
                    {//number board
                    glColor3f(1.0f, 1.0f, 1.0f);//Lilac
                    glRasterPos3f(0,0,0);
                    glRasterPos3f(x_start-0.27,y_start-1.30,-i+0.0001);
                    void* font = GLUT_BITMAP_HELVETICA_10;
                    glutBitmapString(to_string(counter_board+1),font);
                    }
                }

                counter_color++;
                counter_board++;
            }

        }
        if(kanan_kiri==1)
        {
            x_start=x_start-1; //titik tengah acuan
            y_start=y_start+2; //titik tengah acuan

            //1 adalah 1/2 alas segitiga dan 2 adalah tinggi segitiga
            for(int j = 0; j<= batas; j++)
            {
                titik_tengah[counter_board][3]=i;
                if (j==batas)
                    titik_tengah[counter_board][4]=1;
                else
                    titik_tengah[counter_board][4]=0;

                int s=counter_color%6;
                if(j%2==1)
                    {
                    titik_tengah[counter_board][0]=x_start-1;
                    titik_tengah[counter_board][1]=y_start;
                    titik_tengah[counter_board][2]=-i;

                    if(teleport[counter_board]==1)
                    {
                    glColor3f(0.0f, 0.0f, 0.0f);//Lilac
                    glBegin(GL_QUADS);
                    //SERONG KANAN x
                    glVertex3f(x_start-0.7,y_start-0.8,-i);
                    glVertex3f(x_start-0.9,y_start-0.8,-i);
                    glVertex3f(x_start-1.3,y_start-1.3,-i);
                    glVertex3f(x_start-1.1,y_start-1.3,-i);
                     //SERONG KIRI x
                    glVertex3f(x_start-1.1,y_start-0.8,-i);
                    glVertex3f(x_start-1.3,y_start-0.8,-i);
                    glVertex3f(x_start-0.9,y_start-1.3,-i);
                    glVertex3f(x_start-0.7,y_start-1.3,-i);
                    glEnd();
                    }

                    //segitiga terbalik
                    glColor3f(0.0,0.5,0.0);
                    glColor3f(color[s][0],color[s][1],color[s][2]);
                    glBegin(GL_TRIANGLES); //tutup
                    glVertex3f(x_start,y_start,-i);
                    glVertex3f(x_start-1,y_start-2,-i);
                    glVertex3f(x_start-2,y_start,-i);
                    glEnd();
                    glBegin(GL_TRIANGLES); //alas
                    glVertex3f(x_start,y_start,-i-1);
                    glVertex3f(x_start-1,y_start-2,-i-1);
                    glVertex3f(x_start-2,y_start,-i-1);
                    glEnd();
                    glBegin(GL_QUADS);
                    glVertex3f(x_start,y_start,-i);
                    glVertex3f(x_start,y_start,-i-1);
                    glVertex3f(x_start-1,y_start-2,-i-1);
                    glVertex3f(x_start-1,y_start-2,-i);
                    glEnd();
                    glBegin(GL_QUADS);
                    glVertex3f(x_start-1,y_start-2,-i);
                    glVertex3f(x_start-1,y_start-2,-i-1);
                    glVertex3f(x_start-2,y_start,-i-1);
                    glVertex3f(x_start-2,y_start,-i);
                    glEnd();
                    glBegin(GL_QUADS);
                    glVertex3f(x_start-2,y_start,-i);
                    glVertex3f(x_start-2,y_start,-i-1);
                    glVertex3f(x_start,y_start,-i-1);
                    glVertex3f(x_start,y_start,-i);
                    glEnd();
                    if(idle==false)
                        {//number board
                        glColor3f(1.0f, 1.0f, 1.0f);//Lilac
                        glRasterPos3f(0,0,0);
                        glRasterPos3f(x_start-0.27-1,y_start-1.30,-i+0.0001);
                        void* font = GLUT_BITMAP_HELVETICA_10;
                        glutBitmapString(to_string(counter_board+1),font);
                        }
                    x_start=x_start-2;
                    }
                else
                {
                titik_tengah[counter_board][0]=x_start;
                titik_tengah[counter_board][1]=y_start;
                titik_tengah[counter_board][2]=-i;

                if(teleport[counter_board]==1)
                {
                glColor3f(0.0f, 0.0f, 0.0f);//Lilac
                glBegin(GL_QUADS);
                //SERONG KANAN x
                glVertex3f(x_start+0.3,y_start-0.8,-i);
                glVertex3f(x_start+0.1,y_start-0.8,-i);
                glVertex3f(x_start-0.1,y_start-1.3,-i);
                glVertex3f(x_start-0.3,y_start-1.3,-i);
                 //SERONG KIRI x
                glVertex3f(x_start-0.3,y_start-0.8,-i);
                glVertex3f(x_start-0.1,y_start-0.8,-i);
                glVertex3f(x_start+0.1,y_start-1.3,-i);
                glVertex3f(x_start+0.3,y_start-1.3,-i);
                glEnd();
                }

                //segitiga biasa
                glColor3f(0.5,0.0,0.0);
                glColor3f(color[s][0],color[s][1],color[s][2]);
                glBegin(GL_TRIANGLES); //alas
                glVertex3f(x_start,y_start,-i);
                glVertex3f(x_start-1,y_start-2,-i);
                glVertex3f(x_start+1,y_start-2,-i);
                glEnd();
                glBegin(GL_TRIANGLES); //alas
                glVertex3f(x_start,y_start,-i-1);
                glVertex3f(x_start-1,y_start-2,-i-1);
                glVertex3f(x_start+1,y_start-2,-i-1);
                glEnd();
                glBegin(GL_QUADS);
                glVertex3f(x_start,y_start,-i);
                glVertex3f(x_start,y_start,-i-1);
                glVertex3f(x_start-1,y_start-2,-i-1);
                glVertex3f(x_start-1,y_start-2,-i);
                glEnd();
                glBegin(GL_QUADS);
                glVertex3f(x_start-1,y_start-2,-i);
                glVertex3f(x_start-1,y_start-2,-i-1);
                glVertex3f(x_start+1,y_start-2,-i-1);
                glVertex3f(x_start+1,y_start-2,-i);
                glEnd();
                glBegin(GL_QUADS);
                glVertex3f(x_start+1,y_start-2,-i);
                glVertex3f(x_start+1,y_start-2,-i-1);
                glVertex3f(x_start,y_start,-i-1);
                glVertex3f(x_start,y_start,-i);
                glEnd();
                if(idle==false)
                    {//number board
                    glColor3f(1.0f, 1.0f, 1.0f);//Lilac
                    glRasterPos3f(0,0,0);
                    glRasterPos3f(x_start-0.27,y_start-1.30,-i+0.0001);
                    void* font = GLUT_BITMAP_HELVETICA_10;
                    glutBitmapString(to_string(counter_board+1),font);
                    }
                }
                counter_color++;
                counter_board++;
            }

        }
        kanan_kiri++;
        batas=batas-2;
        counter_color++;
    }
}

//{9.0,16.0,-7.0,7,0.0,93}; //XP YP Score_P Baris_ke Up
//BSP batas bergerak samping awalnya 18 lalu dikurangi 2 saat naik (Batas Samping Player)
//Up status tell player go up or left_right
float P[2][6]={{0.0,2.0,0.0,0,0.0,0},{0.0,2.0,0.0,0,0.0,0}}; //XP YP ZP Baris_ke Up Score_P
float Anchor_P[2][3]={{0.0,-0.7,0.0},{0.0,-1.3,0.0}};
int step;
int corak_player=0;float corak_player_timer=0;

float color_spin_dice[3][3]={{1.8,0.2,1.8},{0.2,1.8,1.8},{1.8,1.8,0.2}};
float angle_dice=0;float angle_speed=0.0;float angle_speed_random_max=0;
bool dadu_cepat=true;bool dadu_lambat=false;bool dadu_animation=false;bool dadu_finish_animation=false;int Jdadu=5;
void spin_dice(float angle_dice){
    float PI = 3.1415926535897932384626433832795;
    float R = 3;
    float Teta;
    float x0, y0, x1, y1, x2, y2, xL, yL;
    Teta = (2*PI)/Jdadu;
    glPushMatrix();
    glTranslatef(-7,6,-20);

    glRotatef(angle_dice,0.0,0.0,1.0);
    //glColor3f( 0, 1, 0 );
    //start drawing a line loop
    for(int i = 0; i< Jdadu; i++){
        x1 = R *cos(i*Teta);
        y1 = R *sin(i*Teta);
        x2 = R *cos((i+1)*Teta);
        y2 = R *sin((i+1)*Teta);
        glColor4f(color_spin_dice[i%3][0],color_spin_dice[i%3][1],color_spin_dice[i%3][2],0.0);//orange/brown
        glPushMatrix();
        glBegin(GL_TRIANGLES);
        glVertex3f( 0.0, 0.0, 0.0);
        glVertex3f(x1,y1, 0.0);
        glVertex3f( x2,y2, 0.0);
        glEnd();

        xL = R/2 *cos(i*Teta);
        yL = R/2 *sin(i*Teta);

        glRotatef(360/(Jdadu*2),0.0,0.0,1.0);
        glTranslatef(xL,yL,0.0);
        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);//orange/brown
        if(i==Jdadu-1)
        {
             glColor4f(0.25f, 0.25f, 1.25f, 0.0f);//orange/brown
        }
        void* font = GLUT_BITMAP_TIMES_ROMAN_24;
        glRasterPos3f(0,0,0.001);
        glutBitmapString(to_string(i+1),font);
        glPopMatrix();
    }
    glPopMatrix();
    //panah penunjuk dadu
    glTranslatef(-7,6,-19.9999);
    Teta = (2*PI)/4;
    x1 = R/2 *cos(1*Teta);
    y1 = R/2 *sin(1*Teta);
    glColor4f(0.25f, 0.25f, 0.25f, 0.0f);//orange/brown
     glBegin(GL_TRIANGLES);
        glVertex3f( -0.5, 0.0, 0.0);
        glVertex3f( 0.5, 0.0, 0.0);
        glVertex3f(x1,y1, 0.0);
     glEnd();
}

void teleport_func_player(int S_player,int pn){
    for(int i = 0; i<arraySize; i++)
        {
            int acak=i;
            if (teleport_point[i]==S_player)
            {
                while(true)
                    {
                    int acak = rand() % arraySize;
                    if (acak!=i)
                        {
                            S_player=teleport_point[acak];
                            P[pn][5]=S_player;
                            P[pn][4]=titik_tengah[S_player][4];
                            P[pn][3]=titik_tengah[S_player][3];
                            P[pn][0]=titik_tengah[S_player][0];
                            P[pn][1]=titik_tengah[S_player][1];
                            P[pn][2]=titik_tengah[S_player][2];
                            break;
                        }
                    }
                i=arraySize; //brreak for

            }
        }
}

void player(float JPias, float R, int pn){
    float PI = 3.1415926535897932384626433832795;
    float Teta;
    float x1, y1, x2, y2;
    int i;
    float tinggi=0.5;
    Teta = (2*PI)/JPias;
    glColor3f(1.0,1.0,1.0);
    glTranslatef(Anchor_board[0],Anchor_board[1],Anchor_board[2]);
    if(idle==true)
        Putar_Arena();
    glTranslatef(P[pn][0]+Anchor_P[pn][0],P[pn][1]+Anchor_P[pn][1],P[pn][2]+Anchor_P[pn][2]);

    for(i = 0; i<= JPias; i++){
        glColor3f(1.0,1.0,1.0);
        x1 = R *cos(i*Teta);
        y1 = R *sin(i*Teta);
        x2 = R *cos((i+1)*Teta);
        y2 = R *sin((i+1)*Teta);
        if (pn == 0)
            if(i%4+corak_player%2==1)
                glColor3f(0.0,0.0,0.0);
        if (pn == 1)
            if(i%2+corak_player%2==1)
                glColor3f(0.0,0.0,0.0);
        glBegin(GL_TRIANGLES);
        glVertex3f( 0.0, 0.0, tinggi);
        glVertex3f(x1,y1, tinggi);
        glVertex3f( x2,y2, tinggi);
        glEnd();
        glBegin(GL_QUADS);
        glVertex3f(x1,y1, 0.0);
        glVertex3f( x2,y2, 0.0);
        glVertex3f(x1,y1, tinggi);
        glVertex3f( x2,y2, tinggi);
        glEnd();
    }
}

void gerak_player(int baris,int S_player,int up,int pn){
    int deret_baris[10]={18,35,50,63,74,83,90,95,98,99};
    int BSP = deret_baris[baris];
    if (baris%2==0) //serong atas kiri atau kanan
    {
        if(P[pn][0]<titik_tengah[S_player][0] or P[pn][1]<titik_tengah[S_player][1] or P[pn][2]>titik_tengah[S_player][2]) //if x and y < titik_tengah melakukan animasi
        {
            if(up==0)
            {if(P[pn][0]<titik_tengah[BSP][0]-0.001) //ke kanan
                {P[pn][0]=P[pn][0]+0.05;}
             else
                {P[pn][4]=1;}
            }
            else if(up==1)
            {
            if(P[pn][1]<titik_tengah[BSP+1][1]-0.001) //serong kiri
                {
                 P[pn][0]=P[pn][0]-0.025;
                 P[pn][1]=P[pn][1]+0.05;
                 }
            else if(P[pn][2]>titik_tengah[BSP+1][2]+0.001)
                {
                    P[pn][2]=P[pn][2]-0.025;
                }
            else //case dari 0-ke 36
                {
                P[pn][4]=0;
                baris++;
                P[pn][3]=baris;
                P[pn][0]=titik_tengah[BSP+1][0];
                P[pn][1]=titik_tengah[BSP+1][1];
                P[pn][2]=titik_tengah[BSP+1][2];
                }
            }
        }
        else if(P[pn][0]==titik_tengah[S_player][0] or P[pn][1]==titik_tengah[S_player][1] or P[pn][2]==titik_tengah[S_player][2]){
            if(finish==false)
            {   //back to format animation

                teleport_func_player(S_player,pn);
                if(P[pn][5]<99)
                    animation_player=false;
                if(P[pn][5]>99)
                    ready_go_back=true;
            }
        }

        else //case dari 0 ke 35
        {
        P[pn][4]=0;
        baris++;
        P[pn][3]=baris;
        P[pn][0]=titik_tengah[BSP+1][0];
        P[pn][1]=titik_tengah[BSP+1][1];
        P[pn][2]=titik_tengah[BSP+1][2];
        }
    }
    else if (baris%2==1) //serong atas kanan atau kiri
    {
        if(P[pn][0]>titik_tengah[S_player][0] or P[pn][1]<titik_tengah[S_player][1] or P[pn][2]>titik_tengah[S_player][2]) //if x and y < titik_tengah melakukan animasi
        {
            if(up==0)
            {if(P[pn][0]>titik_tengah[BSP][0]) //ke kiri
                {P[pn][0]=P[pn][0]-0.05;}
             else
                {P[pn][4]=1;}
            }
            else if(up==1)
            {
            if(P[pn][1]<titik_tengah[BSP+1][1]) //serong kanan
                {
                 P[pn][0]=P[pn][0]+0.025;
                 P[pn][1]=P[pn][1]+0.05;
                 }
            else if(P[pn][2]>titik_tengah[BSP+1][2]+0.001)
                {
                    P[pn][2]=P[pn][2]-0.025;
                }
            else
                {
                P[pn][4]=0;
                baris++;
                P[pn][3]=baris;
                P[pn][0]=titik_tengah[BSP+1][0];
                P[pn][1]=titik_tengah[BSP+1][1];
                P[pn][2]=titik_tengah[BSP+1][2];
                }
            }

        }
        else if(P[pn][0]==titik_tengah[S_player][0] or P[pn][1]==titik_tengah[S_player][1] or P[pn][2]==titik_tengah[S_player][2]){
            if (finish==false)
            {   //back to format animation

                teleport_func_player(S_player,pn);
                if(P[pn][5]<99)
                    animation_player=false;
                if(P[pn][5]>99)
                    ready_go_back=true;
            }
        }
        else
        {
        P[pn][4]=0;
        baris++;
        P[pn][3]=baris;
        P[pn][0]=titik_tengah[BSP+1][0];
        P[pn][1]=titik_tengah[BSP+1][1];
        P[pn][2]=titik_tengah[BSP+1][2];
        }
    }

}

void gerak_player_back(int baris,int S_player,int up,int pn){
    int deret_baris[10]={0,19,36,51,64,75,84,91,96,99};
    int BSP = deret_baris[baris];
    if (baris%2==0) //serong bawah kiri atau kiri
    {
        if(P[pn][0]>titik_tengah[S_player][0] or P[pn][1]>titik_tengah[S_player][1] or P[pn][2]<titik_tengah[S_player][2]) //if x and y < titik_tengah melakukan animasi
        {
            if(up==0)
            {
             if(P[pn][0]>titik_tengah[BSP][0]) //ke kiri
                {P[pn][0]=P[pn][0]-0.05;}
             else
                {P[pn][4]=1;}
            }
            else if(up==1)
            {
            if(P[pn][2]<titik_tengah[BSP-1][2]-0.001)
                {
                    P[pn][2]=P[pn][2]+0.025;
                }
            else if(P[pn][1]>=titik_tengah[BSP-1][1]) //serong bawah kiri
                {
                 P[pn][0]=P[pn][0]-0.025;
                 P[pn][1]=P[pn][1]-0.05;
                 }
            else //case dari 0-ke 36
                {
                P[pn][4]=0;
                baris--;
                P[pn][3]=baris;
                P[pn][0]=titik_tengah[BSP-1][0];
                P[pn][1]=titik_tengah[BSP-1][1];
                P[pn][2]=titik_tengah[BSP-1][2];
                }
            }
        }
        else if(P[pn][0]==titik_tengah[S_player][0] or P[pn][1]==titik_tengah[S_player][1] or P[pn][2]==titik_tengah[S_player][2]){
            if (finish==false)
            {//back to format animation
                balik=false;

                teleport_func_player(S_player,pn);
                animation_player=false;
            }
        }
        else //case dari 0 ke 35
        {
        P[pn][4]=0;
        baris--;
        P[pn][3]=baris;
        P[pn][0]=titik_tengah[BSP-1][0];
        P[pn][1]=titik_tengah[BSP-1][1];
        P[pn][2]=titik_tengah[BSP-1][2];
        }
    }
    else if (baris%2==1) //serong atas kanan atau kanan
    {
        if(P[pn][0]<titik_tengah[S_player][0] or P[pn][1]>titik_tengah[S_player][1] or P[pn][2]<titik_tengah[S_player][2]) //if x and y < titik_tengah melakukan animasi
        {
            if(up==0)
            {if(P[pn][0]<titik_tengah[BSP][0]) //ke kanan
                {P[pn][0]=P[pn][0]+0.05;}
             else
                {P[pn][4]=1;}
            }
            else if(up==1)
            {
            if(P[pn][2]<titik_tengah[BSP-1][2]-0.001)
                {
                    P[pn][2]=P[pn][2]+0.025;
                }
            else if(P[pn][1]>=titik_tengah[BSP-1][1]) //serong kanan
                {
                 P[pn][0]=P[pn][0]+0.025;
                 P[pn][1]=P[pn][1]-0.05;
                 }
            else
                {
                P[pn][4]=0;
                baris--;
                P[pn][3]=baris;
                P[pn][0]=titik_tengah[BSP-1][0];
                P[pn][1]=titik_tengah[BSP-1][1];
                P[pn][2]=titik_tengah[BSP-1][2];
                }
            }

        }
        else if(P[pn][0]==titik_tengah[S_player][0] or P[pn][1]==titik_tengah[S_player][1] or P[pn][2]==titik_tengah[S_player][2]){
            if (finish==false)
            {
            //back to format animation
            balik=false;

            teleport_func_player(S_player,pn);
            animation_player=false;
            }
        }
        else
        {
        P[pn][4]=0;
        baris--;
        P[pn][3]=baris;
        P[pn][0]=titik_tengah[BSP-1][0];
        P[pn][1]=titik_tengah[BSP-1][1];
        P[pn][2]=titik_tengah[BSP-1][2];
        }
    }
}

int convert_angle_to_step(float angle_dice){
    if(angle_dice>18 and angle_dice<90)
        return 1;
    if(angle_dice>90 and angle_dice<162)
        return 5;
    if(angle_dice>162 and angle_dice<234)
        return 4;
    if(angle_dice>234 and angle_dice<306)
        return 3;
    if(angle_dice>306 or angle_dice<18)
        return 2;
    if(angle_dice==18 or angle_dice==90 or angle_dice==162 or angle_dice==234 or angle_dice==306)
        {
            int random = rand() % 2 ;
            if(random==0 and angle_dice==18)
                return 2;
            if(random==1 and angle_dice==18)
                return 1;
            if(random==0 and angle_dice==90)
                return 1;
            if(random==1 and angle_dice==90)
                return 5;
            if(random==0 and angle_dice==162)
                return 5;
            if(random==1 and angle_dice==162)
                return 4;
            if(random==0 and angle_dice==234)
                return 4;
            if(random==1 and angle_dice==234)
                return 3;
            if(random==0 and angle_dice==234)
                return 3;
            if(random==1 and angle_dice==306)
                return 2;
        }
}

void dadu_func(){
    if(dadu_animation==true)
    {

        if(pressed==true and dadu_cepat==true and dadu_finish_animation==false and pressed_again==false)
        {

            angle_dice=angle_dice+angle_speed;
            if(angle_dice>=360){ angle_dice=0;}
            if(angle_speed>=10){ angle_speed=10;}
            else{angle_speed=angle_speed+0.1;}
            //angle_dice=90;
            //angle_speed=0;
        }
        else if(pressed_again==true and dadu_cepat==true and dadu_finish_animation==false) //press after angle_speed=10
            {
                dadu_cepat=false;
                dadu_lambat=true;
            }
        else if(angle_speed >0 and dadu_lambat==true and dadu_finish_animation==false)
        {
            angle_speed=angle_speed-0.1;
            if(angle_dice>=360){ angle_dice=0;}
            angle_dice=angle_dice+angle_speed;

        }
         else if(angle_speed <0 and dadu_lambat==true and dadu_finish_animation==false)
            {
                dadu_cepat=true;
                dadu_lambat=false;
                dadu_finish_animation=true;
            }
    }
}

int turn=0;
void turn_player(int pn){
    dadu_func();
    int t=pn*2; //%4 karena ada 2 player
    if (turn%4==t and finish==false and animation_player==false)
    {
        if (pressed==true and dadu_animation==false) //dadu animation continue on dadu_func)
        {
            angle_speed_random_max = 10;
            angle_speed=0.5;
            dadu_animation = true;
        }
        if (pressed==true and  dadu_finish_animation==true)
        {
            //if angle_speed = 0; calc step
            animation_player=true;
            //dice back to format
            dadu_animation=false;
            dadu_finish_animation=false;
            angle_speed=0;

            step=convert_angle_to_step(angle_dice);
            P[pn][5]= step+P[pn][5];
            if (step==Jdadu){
                turn_again=true;
                pressed=false;
                pressed_again=false;
            }
            if (P[pn][5] ==99 and balik==false)
            {
                finish=true;
            }
            turn++;
        }
    }
    else if (turn%4==t+1) //%4 karena ada 2 player
    {
        int up =P[pn][4]; //status go up or go left right
        int S_player=P[pn][5];
        int baris = P[pn][3]; //untuk arah pergerakan player
        if(P[pn][5]<=99 and balik==false)
        {
            gerak_player(baris,S_player,up,pn);
        }
        else if(P[pn][5]>99 and balik==false)
        {
            int skor_lebih=P[pn][5];//saat bergerak nilai akan berubah
            //int save_turn=turn;

            gerak_player(baris,99,up,pn);
            if(ready_go_back==true)
            {
            ready_go_back=false;
            balik=true;
            //turn--; //pada gerak player turn++ saat mencapai 99
            pressed=true; //pada gerak player pressed=false saat mencapai 99
            P[pn][5]=99-(skor_lebih-99);
            P[pn][3]=9;
            P[pn][4]=1;
            }
            //karena dia balik maka tetap giliran untuk animasi
        }
        if(balik==true)
        {
            S_player=P[pn][5];
            gerak_player_back(baris,S_player,up,pn);
        }
        if(animation_player==false and turn_again==false)
        {
            pressed=false;
            pressed_again=false;
            turn++;
        }
        else if(animation_player==false and turn_again==true)
        {
            turn--;
            turn_again=false;
        }

    }
}

int player_number_from_turn(int turn){
    int player_number;
    if (turn%4==0 or turn%4==1)
        player_number=0;
    if (turn%4==2 or turn%4==3)
        player_number=1;
    return player_number;
}

void keyboard(unsigned char key, int x, int y) {
   //printf("%c \n",key);
   switch (key) {
      case 27:     // ESC key
         exit(0);
         break;
      case 32:
          if(idle==true)
            {
             time_idle_animate=0;
             user_x_angle_rotate=0.0;
             user_y_angle_rotate=0.0;
             user_z_angle_rotate=0.0;
             idle=false;
            }
          else if(finish==false and pressed==false and pressed_again==false)
          {
           pressed=true;
          }
          else if(finish==false and pressed==true and pressed_again==false and angle_speed>=10)
          {
           pressed_again=true;
          }
          break;
      case 119: //putar atas ->w
          user_x_angle_rotate+=0.8;
          break;
      case 115: //putar bawah ->s
          user_x_angle_rotate-=0.8;
          break;
      case 100://d putar ke  kanan ->d
          user_y_angle_rotate+=0.8;
          break;
      case 97://a putar ke  kiri ->a
          user_y_angle_rotate-=0.8;
          break;
      case 122:
          // ke kanan searah sumbu z ->z
          user_z_angle_rotate-=0.8;
          break;
      case 120:
          // ke kanan searah sumbu z ->x
          user_z_angle_rotate+=0.8;
          break;
   }
}

void Sentence_turn_roll(int pn){
    glColor3f(0.8f, 0.7f, 1.8f);//Lilac
    void* font = GLUT_BITMAP_TIMES_ROMAN_24;
    glPushMatrix();
    glTranslatef(6,3,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Turn For",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Player",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString(to_string(pn+1),font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,-1,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Press Space",font);
    glPopMatrix();
}

void Sentence_choose_dice(int pn){
    glColor4f(0.0f, 0.5f, 1.0f, 0.0f);//orange/brown
    void* font = GLUT_BITMAP_TIMES_ROMAN_24;
    glPushMatrix();
    glTranslatef(6,3,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Stop Dice For",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Player",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString(to_string(pn+1),font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,-1,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Press Space",font);
    glPopMatrix();
}

void Sentence_turn_Animate(int pn){
    glColor3f(0.8f, 1.8f, 0.7f);//Lilac
    void* font = GLUT_BITMAP_TIMES_ROMAN_24;
    glPushMatrix();
    glTranslatef(6,3,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Animate For",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Player",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString(to_string(pn+1),font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,1,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("GOT",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9,1,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString(to_string(step),font);
    glPopMatrix();
}

void Sentence_turn_release_idle(){
    glColor3f(1.8f, 0.8f, 0.7f);//Lilac
    void* font = GLUT_BITMAP_TIMES_ROMAN_24;
    //glLoadIdentity();
    glPushMatrix();
    glTranslatef(6,3,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Space For",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Play Again",font);
    glPopMatrix();
}

void Sentence_turn_Finish(int pn){
    glColor4f(1.0f, 0.5f, 0.0f, 0.0f);//orange/brown
    void* font = GLUT_BITMAP_TIMES_ROMAN_24;
    glPushMatrix();
    glTranslatef(6,3,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Finish For",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Player",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString(to_string(pn+1),font);
    glPopMatrix();
}
/* Callback handler for normal-key event */
void Sentence_turn_Again(int pn){
    glColor4f(1.0f, 0.5f, 0.0f, 0.0f);//orange/brown
    void* font = GLUT_BITMAP_TIMES_ROMAN_24;
    glPushMatrix();
    glTranslatef(6,3,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Ready For",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Player",font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9,2,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString(to_string(pn+1),font);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(6,1,-20);
    glRasterPos3f(0,0,0);
    glutBitmapString("Turn Again",font);
    glPopMatrix();
}

void init(){
    glClearColor(0.0,0.0,0.0,1.0);
    glEnable(GL_DEPTH_TEST);
    teleport_settings();
}

int main(int argc,char**argv){
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(625,625);
    glutCreateWindow("Window 1");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutTimerFunc(1000,timer,0);
    glutKeyboardFunc(keyboard);
    init();
    glutMainLoop();
}

void display(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();


    //Spin Dice
     if(idle==false){
        glPushMatrix();
        spin_dice(angle_dice);
        glPopMatrix();
    }
    glPushMatrix();
     //Sentence Side Board
    int player_number=player_number_from_turn(turn);
    if (finish==true)
    {
        Sentence_turn_Finish(player_number);
        time_idle_animate=0;
    }
    else if (animation_player==false and idle==false and pressed==false)
    {
        //time_idle_animate=0;
        Sentence_turn_roll(player_number);
    }
    else if (animation_player==false and idle==false and pressed_again==false and angle_speed==10)
    {
        time_idle_animate=0;
        Sentence_choose_dice(player_number);
    }
    else if (animation_player==true and idle==false and pressed_again==true)
        {
            Sentence_turn_Animate(player_number);
            time_idle_animate=0;
        }
    else if (idle==true)
        {
            Sentence_turn_release_idle();
        }
    else if (turn_again==true)
    {
        Sentence_turn_Again(player_number);
        time_idle_animate=0;
    }
    glPopMatrix();

    // Player Transformation
    glPushMatrix();
    player(20,0.2,0);
    glPopMatrix();
    glPushMatrix();
    player(20,0.2,1);
    glPopMatrix();

    // Board Transformation
    glPushMatrix();
    board();
    glPopMatrix();




    glutSwapBuffers();
}

void reshape(int w, int h){
    glViewport(0,0,(GLsizei)w,(GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60,1,2.0,50.0);
    glMatrixMode(GL_MODELVIEW);
}

void timer(int){
    //60 FPS
    glutPostRedisplay();
    glutTimerFunc(1000/60,timer,0);

    //idle=true;
    if(time_idle_animate>600 and finish==false and idle==false)
    {
        user_x_angle_rotate=0.0;
        user_y_angle_rotate=0.0;
        user_z_angle_rotate=0.0;
        idle=true;
    }
    //printf("%d\n",time_idle_animate);
    //character pattern move
    corak_player_timer=corak_player_timer+0.25;
    if (corak_player_timer>1)
    {
        corak_player_timer=0;
        corak_player++;
    }

    //character movement
    turn_player(0);
    turn_player(1);
    time_idle_animate++;

}
